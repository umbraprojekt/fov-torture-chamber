import * as minimist from "minimist";
import { ParsedArgs } from "minimist";
import * as path from "path";
import { MrpasJsWrapperFactory } from "./algorithmWrappers/MrpasJsWrapperFactory";
import { MrpasWrapperFactory } from "./algorithmWrappers/MrpasWrapperFactory";
import { PermissiveFovWrapperFactory } from "./algorithmWrappers/PermissiveFovWrapperFactory";
import { RotDiscreteShadowcastingWrapperFactory } from "./algorithmWrappers/RotDiscreteShadowcastingWrapperFactory";
import { RotPreciseShadowcastingWrapperFactory } from "./algorithmWrappers/RotPreciseShadowcastingWrapperFactory";
import { RotRecursiveShadowcastingWrapperFactory } from "./algorithmWrappers/RotRecursiveShadowcastingWrapperFactory";
import { SymmetricShadowcastingWrapperFactory } from "./algorithmWrappers/SymmetricShadowcastingWrapperFactory";
import { AlgorithmsParser } from "./argParsers/AlgorithmsParser";
import { MapsParser } from "./argParsers/MapsParser";
import { TestsParser } from "./argParsers/TestsParser";
import { ImageGenerator } from "./output/ImageGenerator";
import { SummaryGenerator } from "./output/SummaryGenerator";
import { Results } from "./Results";
import { BenchmarkTest } from "./tests/BenchmarkTest";
import { ImageTest } from "./tests/ImageTest";
import { SymmetryImageTest } from "./tests/SymmetryImageTest";
import { SymmetryTest } from "./tests/SymmetryTest";
import { TortureChamber } from "./TortureChamber";

// let's gather the command line arguments
const argv: ParsedArgs = minimist(process.argv.slice(2));

const maps: string = argv.m || argv.maps || "all";
const tests: string = argv.t || argv.tests || "all";
const algorithms: string = argv.a || argv.algorithms || "all";
const benchTime: number | string = argv.T || argv.time || 5000;

// we'll need this in a moment
const imageGenerator: ImageGenerator = new ImageGenerator();

// parsers
const algorithmsParser: AlgorithmsParser = new AlgorithmsParser([
	new RotDiscreteShadowcastingWrapperFactory(),
	new RotPreciseShadowcastingWrapperFactory(),
	new RotRecursiveShadowcastingWrapperFactory(),
	new MrpasJsWrapperFactory(),
	new SymmetricShadowcastingWrapperFactory(),
	new MrpasWrapperFactory(),
	new PermissiveFovWrapperFactory()
]);
const testsParser: TestsParser = new TestsParser([
	new BenchmarkTest(parseInt(benchTime as string, 10)),
	new ImageTest(imageGenerator),
	new SymmetryTest(),
	new SymmetryImageTest(imageGenerator)
]);
const mapsParser: MapsParser = new MapsParser();

// initialise the torture chamber
const tortureChamber: TortureChamber = new TortureChamber(
	mapsParser.parse(maps),
	algorithmsParser.parse(algorithms),
	testsParser.parse(tests)
);

// let the pain commence!
(async () => {
	try {
		await tortureChamber.run();
	} catch (e) {
		process.exit(1);
	}

	const results: Results = tortureChamber.generateResults();
	new SummaryGenerator().generate(results, path.normalize(`${__dirname}/../results/summary.md`));
	process.exit(0);
})();
