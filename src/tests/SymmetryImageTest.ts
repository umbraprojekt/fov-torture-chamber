import * as path from "path";
import { AlgorithmWrapper } from "../algorithmWrappers/AlgorithmWrapper";
import { FovMap } from "../FovMap";
import { ImageGenerator } from "../output/ImageGenerator";
import { Test } from "./Test";

export class SymmetryImageTest implements Test {

	public readonly name: string = "FOV image with symmetry generation";
	public readonly normalisedName: string = "symmetryImage";

	public constructor(
		private readonly imageGenerator: ImageGenerator
	) {}

	public async run(map: FovMap, algorithm: AlgorithmWrapper): Promise<void> {}

	public async runAndSaveResult(map: FovMap, algorithm: AlgorithmWrapper): Promise<void> {
		// repeated code, but I wanted to avoid dependency on SymmetryTest
		map.resetOriginVisibility();
		map.resetVisibility();

		// generate origin visibility from all cells (including walls)
		map.forEachFloorCell((x: number, y: number) => {
			algorithm.compute(x, y);
			if (map.isVisible(map.originX, map.originY)) {
				map.setOriginVisibleFrom(x, y);
			}
			map.resetVisibility();
		});

		// calculate visibility from
		algorithm.compute(map.originX, map.originY);

		const outputPath = path.normalize(`${__dirname}/../../results/symmetry/${map.name}-${algorithm.normalisedName}.png`);
		await this.imageGenerator.generateWithSymmetry(map, outputPath);
	}
}
