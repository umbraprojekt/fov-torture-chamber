import * as path from "path";
import { AlgorithmWrapper } from "../algorithmWrappers/AlgorithmWrapper";
import { FovMap } from "../FovMap";
import { ImageGenerator } from "../output/ImageGenerator";
import { Test } from "./Test";

export class ImageTest implements Test {

	public readonly name: string = "FOV image generation";
	public readonly normalisedName: string = "image";

	public constructor(
		private readonly imageGenerator: ImageGenerator
	) {}

	public async run(map: FovMap, algorithm: AlgorithmWrapper): Promise<void> {}

	public async runAndSaveResult(map: FovMap, algorithm: AlgorithmWrapper): Promise<void> {
		map.resetVisibility();
		algorithm.compute(map.originX, map.originY);

		let outputPath = path.normalize(`${__dirname}/../../results/fov/${map.name}-${algorithm.normalisedName}.png`);
		await this.imageGenerator.generateWithoutSymmetry(map, outputPath);
	}
}
