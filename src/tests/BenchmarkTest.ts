import { AlgorithmWrapper } from "../algorithmWrappers/AlgorithmWrapper";
import { FovMap } from "../FovMap";
import { AlgorithmBenchmark, Benchmark } from "../Results";
import { Test } from "./Test";

export class BenchmarkTest implements Test {

	public readonly name: string = "Algorithm benchmark";
	public readonly normalisedName: string = "benchmark";
	public readonly results: Array<AlgorithmBenchmark> = [];

	public constructor(
		private benchmarkTime: number
	) {}

	public async run(map: FovMap, algorithm: AlgorithmWrapper): Promise<void> {
		this.runAndGenerateBenchmark(algorithm, map);
	}

	public async runAndSaveResult(map: FovMap, algorithm: AlgorithmWrapper): Promise<void> {
		const benchmark: Benchmark = this.runAndGenerateBenchmark(algorithm, map);
		this.addBenchmarkToResults(benchmark, algorithm);
	}

	private addBenchmarkToResults(result: Benchmark, algorithm: AlgorithmWrapper): void {
		let algorithmBenchmark: AlgorithmBenchmark | undefined = this.results
			.find((item: AlgorithmBenchmark) => item.algorithm === algorithm.name);
		if (!algorithmBenchmark) {
			algorithmBenchmark = {
				algorithm: algorithm.name,
				results: []
			};
			this.results.push(algorithmBenchmark);
		}

		algorithmBenchmark.results.push(result);
	}

	private runAndGenerateBenchmark(wrapper: AlgorithmWrapper, map: FovMap): Benchmark {
		let iterations: number;
		let elapsed: number;
		const start: number = new Date().valueOf();
		for (iterations = 0, elapsed = 0; elapsed < this.benchmarkTime; ++iterations) {
			wrapper.compute(map.originX, map.originY);
			++iterations;
			elapsed = new Date().valueOf() - start;
		}

		return {
			map: map.name,
			iterations,
			elapsed,
			average: elapsed / iterations
		};
	}
}
