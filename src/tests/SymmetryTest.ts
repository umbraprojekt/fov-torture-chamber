import { AlgorithmWrapper } from "../algorithmWrappers/AlgorithmWrapper";
import { FovMap } from "../FovMap";
import { AlgorithmSymmetry, Symmetry } from "../Results";
import { Test } from "./Test";

export class SymmetryTest implements Test {

	public readonly name: string = "Symmetry calculation";
	public readonly normalisedName: string = "symmetry";
	public readonly results: Array<AlgorithmSymmetry> = [];

	public async run(map: FovMap, algorithm: AlgorithmWrapper): Promise<void> {
		map.resetOriginVisibility();
		map.resetVisibility();

		// generate origin visibility from all cells (including walls)
		map.forEachFloorCell((x: number, y: number) => {
			algorithm.compute(x, y);
			if (map.isVisible(map.originX, map.originY)) {
				map.setOriginVisibleFrom(x, y);
			}
			map.resetVisibility();
		});

		// calculate visibility from
		algorithm.compute(map.originX, map.originY);
	}

	public async runAndSaveResult(map: FovMap, algorithm: AlgorithmWrapper): Promise<void> {
		this.run(map, algorithm);

		const symmetry: Symmetry = this.generateSymmetry(map);
		this.addSymmetryToResults(symmetry, algorithm);
	}

	private addSymmetryToResults(result: Symmetry, algorithm: AlgorithmWrapper): void {
		let algorithmSymmetry: AlgorithmSymmetry | undefined = this.results
			.find((item: AlgorithmSymmetry) => item.algorithm === algorithm.name);
		if (!algorithmSymmetry) {
			algorithmSymmetry = {
				algorithm: algorithm.name,
				results: []
			};
			this.results.push(algorithmSymmetry);
		}

		algorithmSymmetry.results.push(result);
	}

	private generateSymmetry(map: FovMap): Symmetry {
		const result: Symmetry = {
			map: map.name,
			cells: 0,
			errors: 0,
			errorPercentage: 0
		};
		map.forEachFloorCell((x: number, y: number) => {
			if (x === map.originX && y === map.originY) {
				return;
			}

			result.cells++;

			if (map.isVisible(x, y) !== map.isOriginVisibleFrom(x, y)) {
				result.errors++;
			}
		});
		result.errorPercentage = result.cells === 0 ? 0 : 100 * result.errors / result.cells;

		return result;
	}
}
