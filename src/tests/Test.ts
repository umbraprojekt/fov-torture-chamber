import { AlgorithmWrapper } from "../algorithmWrappers/AlgorithmWrapper";
import { FovMap } from "../FovMap";

export interface Test {
	name: string;
	normalisedName: string;
	run(map: FovMap, algorithm: AlgorithmWrapper): Promise<void>;
	runAndSaveResult(map: FovMap, algorithm: AlgorithmWrapper): Promise<void>;
}
