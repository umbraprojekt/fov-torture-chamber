export interface Benchmark {
	map: string;
	iterations: number;
	elapsed: number;
	average: number;
}

export interface AlgorithmBenchmark {
	algorithm: string;
	results: Array<Benchmark>;
}

export interface Symmetry {
	map: string;
	cells: number;
	errors: number;
	errorPercentage: number;
}

export interface AlgorithmSymmetry {
	algorithm: string;
	results: Array<Symmetry>;
}

export interface Results {
	benchmark?: Array<AlgorithmBenchmark>;
	symmetry?: Array<AlgorithmSymmetry>;
}
