import { Test } from "../tests/Test";

export class TestsParser {

	public constructor(
		private readonly tests: Array<Test>
	) {}

	public parse(arg: string): Array<Test> {
		const testNames: Array<string> = arg.split(",");
		const result: Array<Test> = [];

		testNames.forEach((testName: string) => {
			switch (testName) {
				case "all":
					this.tests.forEach((item: Test) => result.push(item));
					break;
				default:
					const test: Test | undefined = this.tests.find((item: Test) => item.normalisedName === testName);
					if (test) {
						result.push(test);
					} else {
						throw new Error(`Unkown test: ${testName}`);
					}
					break;
			}
		});

		return result
			.filter((item: Test, pos: number, self: Array<Test>) => self.indexOf(item) === pos);
	}
}
