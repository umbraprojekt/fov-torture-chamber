import { AlgorithmWrapperFactory } from "../algorithmWrappers/AlgorithmWrapperFactory";

export class AlgorithmsParser {

	public constructor(
		private readonly algorithmFactories: Array<AlgorithmWrapperFactory>
	) {}

	public parse(arg: string): Array<AlgorithmWrapperFactory> {
		const algorithmNames: Array<string> = arg.split(",");
		const result: Array<string> = [];

		algorithmNames.forEach((algorithmName: string) => {
			switch (algorithmName) {
				case "all":
					this.algorithmFactories.forEach((item: AlgorithmWrapperFactory) => result.push(item.name));
					break;
				default:
					if (!!this.algorithmFactories
						.find((item: AlgorithmWrapperFactory) => item.name === algorithmName)) {
						result.push(algorithmName);
					} else {
						throw new Error(`Unkown algorithm: ${algorithmName}`);
					}
					break;
			}
		});

		return result
			.filter((item: string, pos: number, self: Array<string>) => self.indexOf(item) === pos)
			.sort()
			.map((name: string) => {
				return this.algorithmFactories
					.find((item: AlgorithmWrapperFactory) => item.name === name) as AlgorithmWrapperFactory;
			});
	}
}
