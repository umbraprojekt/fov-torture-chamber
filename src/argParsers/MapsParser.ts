import * as fs from "fs";
import * as path from "path";
import { FovMap } from "../FovMap";

export class MapsParser {

	private readonly availableMaps: Array<string>;

	public constructor() {
		const filenames: Array<string> = fs.readdirSync(path.normalize(`${__dirname}/../../maps`));
		this.availableMaps = filenames.map((filename: string) => path.parse(filename).name);
	}

	public parse(arg: string): Array<FovMap> {
		const mapNames: Array<string> = arg.split(",");
		const result: Array<string> = [];

		mapNames.forEach((mapName: string) => {
			switch (mapName) {
				case "all": this.availableMaps
						.forEach((map: string) => result.push(map));
					break;
				case "pillars": this.availableMaps
						.filter((map: string) => map.includes("pillar"))
						.forEach((map: string) => result.push(map));
					break;
				case "indoors": this.availableMaps
						.filter((map: string) => map.includes("indoor"))
						.forEach((map: string) => result.push(map));
					break;
				case "outdoors": this.availableMaps
						.filter((map: string) => map.includes("outdoor"))
						.forEach((map: string) => result.push(map));
					break;
				case "corners": this.availableMaps
						.filter((map: string) => map.includes("corner"))
						.forEach((map: string) => result.push(map));
					break;
				case "base": this.availableMaps
						.filter((map: string) => ["25-percent-1", "25-percent-2",
							"diagonal", "empty", "full"].indexOf(map) > -1)
						.forEach((map: string) => result.push(map));
					break;
				default:
					if (this.availableMaps.indexOf(mapName) === -1) {
						throw new Error(`Unknown map name: ${mapName}`);
					}
					result.push(mapName);
			}
		});

		return result
			.filter((item: string, pos: number, self: Array<string>) => self.indexOf(item) === pos)
			.sort()
			.map((mapName: string) => {
				return new FovMap(path.normalize(`${__dirname}/../../maps/${mapName}.map`));
			});
	}
}
