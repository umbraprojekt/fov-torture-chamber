import { FovMap } from "../FovMap";
import { AlgorithmWrapper } from "./AlgorithmWrapper";
import { AlgorithmWrapperFactory } from "./AlgorithmWrapperFactory";
import { ROT } from "fovs";

export class RotDiscreteShadowcastingWrapperFactory implements AlgorithmWrapperFactory {

	public readonly name: string = "rot-discrete";

	public build(map: FovMap): AlgorithmWrapper {
		const discreteShadowcasting: ROT.FOV.DiscreteShadowcasting = new ROT.FOV.DiscreteShadowcasting(
			(x: number, y: number) => map.isTransparent(x, y)
		);
		const setVisible: ROT.FOV.VisibilityCallback = (x: number, y: number, r: number, visibility: number) => {
			if (visibility === 1) {
				map.setVisible(x, y);
			}
		};

		return {
			name: "Rot.js Discrete Shadowcasting",
			normalisedName: this.name,
			compute(originX: number, originY: number): void {
				discreteShadowcasting.compute(originX, originY, map.maxRange, setVisible);
			}
		};
	}
}
