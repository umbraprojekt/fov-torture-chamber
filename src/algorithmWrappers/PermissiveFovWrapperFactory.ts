import { PermissiveFov } from "permissive-fov";
import { FovMap } from "../FovMap";
import { AlgorithmWrapper } from "./AlgorithmWrapper";
import { AlgorithmWrapperFactory } from "./AlgorithmWrapperFactory";

export class PermissiveFovWrapperFactory implements AlgorithmWrapperFactory {

	public readonly name: string = "permissive";

	public build(map: FovMap): AlgorithmWrapper {
		const permissiveFov: PermissiveFov = new PermissiveFov(map.width, map.height, map.isTransparent.bind(map));
		const setVisible: (x: number, y: number) => void = map.setVisible.bind(map);

		return {
			name: "Permissive FOV",
			normalisedName: this.name,
			compute(originX: number, originY: number): void {
				permissiveFov.compute(originX, originY, map.maxRange, setVisible);
			}
		};
	}
}
