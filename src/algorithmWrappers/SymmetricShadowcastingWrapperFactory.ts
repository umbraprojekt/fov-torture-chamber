import { FovMap } from "../FovMap";
import { AlgorithmWrapper } from "./AlgorithmWrapper";
import { AlgorithmWrapperFactory } from "./AlgorithmWrapperFactory";
import { SymmetricShadowcasting } from "fovs";

export class SymmetricShadowcastingWrapperFactory implements AlgorithmWrapperFactory {

	public readonly name: string = "sym-shadow";

	public build(map: FovMap): AlgorithmWrapper {
		const setVisible: (x: number, y: number) => void = map.setVisible.bind(map);
		const isTransparent: (x: number, y: number) => boolean = map.isTransparent.bind(map);

		return {
			name: "Symmetric ShadowCasting (https://gist.github.com/as-f/59bb06ced7e740e11ec7dda9d82717f6)",
			normalisedName: this.name,
			compute(originX: number, originY: number): void {
				SymmetricShadowcasting(originX, originY, isTransparent, setVisible);
			}
		};
	}
}
