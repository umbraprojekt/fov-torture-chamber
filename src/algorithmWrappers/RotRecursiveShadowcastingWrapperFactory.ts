import { FovMap } from "../FovMap";
import { AlgorithmWrapper } from "./AlgorithmWrapper";
import { AlgorithmWrapperFactory } from "./AlgorithmWrapperFactory";
import { ROT } from "fovs";

export class RotRecursiveShadowcastingWrapperFactory implements AlgorithmWrapperFactory {

	public readonly name: string = "rot-recursive";

	public build(map: FovMap): AlgorithmWrapper {
		const recursiveShadowcasting: ROT.FOV.RecursiveShadowcasting = new ROT.FOV.RecursiveShadowcasting(
			(x: number, y: number) => map.isTransparent(x, y)
		);
		const setVisible: ROT.FOV.VisibilityCallback = (x: number, y: number, r: number, visibility: number) => {
			if (visibility === 1) {
				map.setVisible(x, y);
			}
		};

		return {
			name: "Rot.js Recursive Shadowcasting",
			normalisedName: this.name,
			compute(originX: number, originY: number): void {
				recursiveShadowcasting.compute(originX, originY, map.maxRange, setVisible);
			}
		};
	}
}
