import { AlgorithmWrapper } from "./AlgorithmWrapper";
import { FovMap } from "../FovMap";
import { AlgorithmWrapperFactory } from "./AlgorithmWrapperFactory";
import { MrpasJs } from "fovs";

export class MrpasJsWrapperFactory implements AlgorithmWrapperFactory {

	public readonly name: string = "mrpas-js";

	public build(map: FovMap): AlgorithmWrapper {
		// mrpas-js uses its own implementation of the map object and relies on its methods and properties.
		// we will inject our own map there and thus need to add some hairy stuff there
		const mrpasMap: MrpasJs.Map = new MrpasJs.Map([map.width, map.height]);

		mrpasMap.is_visible = (pos: Array<number>): boolean => {
			return map.isVisible(pos[0], pos[1]);
		};
		mrpasMap.set_visible = (pos: Array<number>): void => {
			map.setVisible(pos[0], pos[1]);
		};
		mrpasMap.is_transparent = (pos: Array<number>): boolean => {
			return map.isTransparent(pos[0], pos[1]);
		};
		mrpasMap.reset_visibility = () => {
			map.resetVisibility();
		};

		return {
			name: "MRPAS (https://github.com/domasx2/mrpas-js)",
			normalisedName: this.name,
			compute(originX: number, originY: number): void {
				MrpasJs.compute(mrpasMap, [originX, originY], map.maxRange);
			}
		};
	}
}
