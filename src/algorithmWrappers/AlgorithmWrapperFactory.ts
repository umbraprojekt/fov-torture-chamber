import { FovMap } from "../FovMap";
import { AlgorithmWrapper } from "./AlgorithmWrapper";

export interface AlgorithmWrapperFactory {

	name: string;
	build(map: FovMap): AlgorithmWrapper;
}
