import { FovMap } from "../FovMap";
import { AlgorithmWrapper } from "./AlgorithmWrapper";
import { AlgorithmWrapperFactory } from "./AlgorithmWrapperFactory";
import { ROT } from "fovs";

export class RotPreciseShadowcastingWrapperFactory implements AlgorithmWrapperFactory {

	public readonly name: string = "rot-precise";

	public build(map: FovMap): AlgorithmWrapper {
		const preciseShadowcasting: ROT.FOV.PreciseShadowcasting = new ROT.FOV.PreciseShadowcasting(
			(x: number, y: number) => map.isTransparent(x, y)
		);
		const setVisible: ROT.FOV.VisibilityCallback = (x: number, y: number, r: number, visibility: number) => {
			if (visibility === 1) {
				map.setVisible(x, y);
			}
		};

		return {
			name: "Rot.js Precise Shadowcasting",
			normalisedName: this.name,
			compute(originX: number, originY: number): void {
				preciseShadowcasting.compute(originX, originY, map.maxRange, setVisible);
			}
		};
	}
}
