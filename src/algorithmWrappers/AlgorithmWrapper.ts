export interface AlgorithmWrapper {
	name: string;
	normalisedName: string;
	compute(originX: number, originY: number): void;
}
