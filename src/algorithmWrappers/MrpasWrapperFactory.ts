import { Mrpas } from "mrpas";
import { FovMap } from "../FovMap";
import { AlgorithmWrapper } from "./AlgorithmWrapper";
import { AlgorithmWrapperFactory } from "./AlgorithmWrapperFactory";

export class MrpasWrapperFactory implements AlgorithmWrapperFactory {

	public readonly name: string = "mrpas";

	public build(map: FovMap): AlgorithmWrapper {
		const mrpas: Mrpas = new Mrpas(map.width, map.height, (x: number, y: number) => map.isTransparent(x, y));
		const isVisible: (x: number, y: number) => boolean = map.isVisible.bind(map);
		const setVisible: (x: number, y: number) => void = map.setVisible.bind(map);

		return {
			name: "MRPAS",
			normalisedName: this.name,
			compute(originX: number, originY: number): void {
				mrpas.compute(originX, originY, map.maxRange, isVisible, setVisible);
			}
		};
	}
}
