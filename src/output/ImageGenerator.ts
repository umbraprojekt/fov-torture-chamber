import * as path from "path";
import * as fs  from "fs";
import { FovMap } from "../FovMap";
import * as Jimp from "jimp";

export class ImageGenerator {

	private static readonly TILE_NAMES: Array<string> = [
		"invisible-asymmetry",
		"invisible-floor",
		"invisible-wall",
		"visible-asymmetry",
		"visible-floor",
		"visible-origin",
		"visible-wall"
	];
	private static readonly PROMISES: Array<Promise<Jimp>> = ImageGenerator.TILE_NAMES
		.map((name: string) => Jimp.read(path.join(__dirname, `../../tiles/${name}.png`)));

	private async generate(map: FovMap, filename: string, symmetry: boolean): Promise<void> {
		const tilePromises: Array<Jimp> = await Promise.all(ImageGenerator.PROMISES);
		const tiles: {[key: string]: Jimp} = {};
		const tileWidth: number = tilePromises[0].getWidth();
		const tileHeight: number = tilePromises[0].getHeight();

		ImageGenerator.TILE_NAMES.forEach((name: string, index: number) => tiles[name] = tilePromises[index]);

		const image: Jimp = new Jimp(tileWidth * map.width, tileHeight * map.height);

		map.forEachCell((x: number, y: number, cellChar?: string, visibility?: number): void => {
			const tile: string = [
				visibility ? "visible" : "invisible",
				symmetry && map.isFloor(x, y) && !!visibility !== map.isOriginVisibleFrom(x, y) ?
					"asymmetry" :
					map.getType(x, y)
			].join("-");
			image.blit(tiles[tile], x * tileWidth, y * tileHeight);
		});

		fs.existsSync(path.dirname(filename)) || fs.mkdirSync(path.dirname(filename), { recursive: true });
		await image.writeAsync(filename);
	}

	public async generateWithoutSymmetry(map: FovMap, filename: string): Promise<void> {
		await this.generate(map, filename, false);
	}

	public async generateWithSymmetry(map: FovMap, filename: string): Promise<void> {
		await this.generate(map, filename, true);
	}
}
