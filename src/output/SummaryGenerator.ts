import * as fs from "fs";
import * as path from "path";
import { Results, AlgorithmBenchmark, Benchmark, AlgorithmSymmetry, Symmetry } from "../Results";

// TODO switch to using nunjucks
export class SummaryGenerator {

	public generate(results: Results, filename: string): void {
		let output = "# FOV Torture Chamber - results summary\n\n";

		if (results.benchmark) {
			output += this.generateBenchmarks(results.benchmark);
		}
		if (results.symmetry) {
			output += this.generateSymmetry(results.symmetry);
		}

		fs.existsSync(path.dirname(filename)) || fs.mkdirSync(path.dirname(filename));
		fs.writeFileSync(filename, output);
	}

	private generateBenchmarks(benchmarks: Array<AlgorithmBenchmark>): string {
		let output = "## Benchmarks\n\n";

		// headers
		const headers = ["algorithm / map"].concat(benchmarks[0].results.map((result: Benchmark) => {
			return result.map;
		}));
		const separators = ["---"].concat(benchmarks[0].results.map(() => {
			return "---:";
		}));

		output += headers.join(" | ") + "\n";
		output += separators.join(" | ") + "\n";

		// content
		benchmarks.forEach((item: AlgorithmBenchmark) => {
			const cellsInRow = [item.algorithm].concat(item.results.map((result: Benchmark) => {
				return (result.average * 1000).toFixed(2).toString() + "µs";
			}));
			output += cellsInRow.join(" | ") + "\n";
		});

		output += "\n";

		return output;
	}

	private generateSymmetry(symmetries: Array<AlgorithmSymmetry>): string {
		let output = "## Symmetry\n\n";

		// headers
		const headers = ["algorithm / map"].concat(symmetries[0].results.map((result: Symmetry) => {
			return result.map;
		}));
		const separators = ["---"].concat(symmetries[0].results.map(() => {
			return "---:";
		}));

		output += headers.join(" | ") + "\n";
		output += separators.join(" | ") + "\n";

		// content
		symmetries.forEach((item: AlgorithmSymmetry) => {
			const cellsInRow = [item.algorithm].concat(item.results.map((result: Symmetry) => {
				return result.errorPercentage.toFixed(2).toString() + "%";
			}));
			output += cellsInRow.join(" | ") + "\n";
		});

		output += "\n";

		return output;
	}
}
