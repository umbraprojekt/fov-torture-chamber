import * as fs from "fs";
import * as path from "path";
import * as colors from "colors";

interface ICellCoordinates {
	x: number;
	y: number;
}

export class FovMap {

	private static readonly CELL_CHAR_ORIGIN: string = "@";
	private static readonly CELL_CHAR_FLOOR: string = ".";
	private static readonly CELL_CHAR_WALL: string = "#";
	private static readonly CELL_TYPES: {[key: string]: string} = {
		".": "floor",
		"#": "wall",
		"@": "origin"
	};

	public readonly name: string;
	public cells: Array<string> = [];
	private readonly visibility: Array<number> = [];
	private readonly originVisibility: Array<number> = [];
	public readonly width: number;
	public readonly height: number;
	public readonly originX: number;
	public readonly originY: number;
	public readonly maxRange: number;

	public constructor(filename: string) {
		this.name = path.basename(filename, path.extname(filename));

		const contents: string = fs.readFileSync(filename, "utf8");
		const lines: Array<string> = contents.split("\n");
		lines.pop();

		this.height = lines.length;
		this.width = lines[0].split("").length;
		this.cells = lines.join("").split("");

		const originIdx = this.cells.indexOf(FovMap.CELL_CHAR_ORIGIN);
		this.originX = originIdx % this.width;
		this.originY = Math.floor(originIdx / this.width);

		const maxXRange = Math.max(this.width - this.originX - 1, this.originX);
		const maxYRange = Math.max(this.height - this.originY - 1, this.originY);
		this.maxRange = Math.ceil(Math.sqrt(maxXRange ** 2 + maxYRange ** 2));

		this.resetVisibility();
		this.resetOriginVisibility();
	}

	public resetVisibility(): void {
		for (let y = 0; y < this.height; ++y) {
			for (let x = 0; x < this.width; ++x) {
				this.visibility[y * this.width + x] = 0;
			}
		}
	}

	public resetOriginVisibility(): void {
		for (let y = 0; y < this.height; ++y) {
			for (let x = 0; x < this.width; ++x) {
				this.originVisibility[y * this.width + x] = 0;
			}
		}
	}

	public contains(x: number, y: number): boolean {
		return x >= 0 && x < this.width && y >= 0 && y < this.height;
	}

	public isTransparent(x: number, y: number): boolean {
		return this.contains(x, y) && this.cells[y * this.width + x] != FovMap.CELL_CHAR_WALL;
	}

	public isOpaque(x: number, y: number): boolean {
		return !this.contains(x, y) || this.cells[y * this.width + x] == FovMap.CELL_CHAR_WALL;
	}

	public setVisible(x: number, y: number): void {
		if (this.contains(x, y)) {
			this.visibility[y * this.width + x] = 1;
		}
	}

	public setOriginVisibleFrom(x: number, y: number): void {
		if (this.contains(x, y)) {
			this.originVisibility[y * this.width + x] = 1;
		}
	}

	public isVisible(x: number, y: number): boolean {
		return this.visibility[y * this.width + x] == 1;
	}

	public isOriginVisibleFrom(x: number, y: number): boolean {
		return this.originVisibility[y * this.width + x] == 1;
	}

	public forEachCell(
		callback: (x: number, y: number, cellChar?: string, visibility?: number, sourceVisibility?: number) => void
	): void {
		for (let y = 0; y < this.height; ++y) {
			for (let x = 0; x < this.width; ++x) {
				let idx = y * this.width + x;
				callback(x, y, this.cells[idx], this.visibility[idx], this.originVisibility[idx]);
			}
		}
	}

	public forEachFloorCell(
		callback: (x: number, y: number, cellChar?: string, visibility?: number, sourceVisibility?: number) => void
	): void {
		this.forEachCell((x, y, cellChar, visibility, sourceVisibility) => {
			if (cellChar == FovMap.CELL_CHAR_FLOOR || cellChar == FovMap.CELL_CHAR_ORIGIN) {
				callback(x, y, cellChar, visibility, sourceVisibility);
			}
		});
	}

	public getAllFloorCoordinates(): Array<ICellCoordinates> {
		const floors: Array<ICellCoordinates> = [];
		this.forEachFloorCell((x: number, y: number) => {
			floors.push({x, y});
		});

		return floors;
	}

	public getType(x: number, y: number): string {
		return FovMap.CELL_TYPES[this.cells[y * this.width + x]];
	}

	public isFloor(x: number, y: number): boolean {
		return [
			FovMap.CELL_CHAR_FLOOR,
			FovMap.CELL_CHAR_ORIGIN
		].indexOf(this.cells[y * this.width + x]) > -1;
	}

	public toString(): string {
		const rows: Array<string> = [];
		for (let y = 0; y < this.height; ++y) {
			let row: string = "";
			for (let x = 0; x < this.height; ++x) {
				const char: string = this.cells[y * this.width + x];
				if (this.isVisible(x, y)) {
					row += colors.bgYellow.black(char);
				} else {
					row += colors.bgBlack.grey(char);
				}
			}
			rows.push(row);
		}
		return rows.join("\n");
	}
}
