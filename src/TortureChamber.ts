import * as ProgressBar from "progress";
import { AlgorithmWrapper } from "./algorithmWrappers/AlgorithmWrapper";
import { AlgorithmWrapperFactory } from "./algorithmWrappers/AlgorithmWrapperFactory";
import { FovMap } from "./FovMap";
import { Results } from "./Results";
import { Test } from "./tests/Test";
import { BenchmarkTest } from "./tests/BenchmarkTest";
import { SymmetryTest } from "./tests/SymmetryTest";

export class TortureChamber {

	public constructor (
		private readonly maps: Array<FovMap>,
		private readonly algorithmWrapperFactories: Array<AlgorithmWrapperFactory>,
		private readonly tests: Array<Test>
	) {}

	public async run(): Promise<void> {
		await this.init();

		for (const test of this.tests) {
			console.log(`${test.name}`);
			const progressBar: ProgressBar = new ProgressBar("[:bar] (:percent)", {
				total: this.algorithmWrapperFactories.length * this.maps.length,
				width: 30
			});

			for (const algorithmWrapperFactory of this.algorithmWrapperFactories) {
				for (const map of this.maps) {
					const wrapper: AlgorithmWrapper = algorithmWrapperFactory.build(map);
					await test.runAndSaveResult(map, wrapper);
					progressBar.tick();
				}
			}

			console.log("");
		}
	}

	public generateResults(): Results {
		const results: Results = {};
		for (const test of this.tests) {
			if (test instanceof BenchmarkTest) {
				results.benchmark = test.results;
			}
			if (test instanceof SymmetryTest) {
				results.symmetry = test.results;
			}
		}

		return results;
	}

	private async init(): Promise<void> {
		const benchmark: Test | undefined = this.tests.find((test: Test) => test.normalisedName === "benchmark");
		if (benchmark) {
			// perform benchmark dry run
			await benchmark.run(this.maps[0], this.algorithmWrapperFactories[0].build(this.maps[0]));
		}
	}
}
