"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
require("mocha");
const path = require("path");
const SymmetricShadowcastingWrapperFactory_1 = require("../../src/algorithmWrappers/SymmetricShadowcastingWrapperFactory");
const FovMap_1 = require("../../src/FovMap");
const SymmetryTest_1 = require("../../src/tests/SymmetryTest");
describe("SymmetryCalculator", () => {
    it("should correctly calculate symmetry on 25-percent-1 for symmetric-shadowcasting", () => __awaiter(this, void 0, void 0, function* () {
        // given
        const map = new FovMap_1.FovMap(path.normalize(`${__dirname}/../../maps/25-percent-1.map`));
        const wrapper = new SymmetricShadowcastingWrapperFactory_1.SymmetricShadowcastingWrapperFactory().build(map);
        const calculator = new SymmetryTest_1.SymmetryTest();
        // when
        yield calculator.runAndSaveResult(map, wrapper);
        // then
        chai_1.expect(map.isOriginVisibleFrom(1, 0)).to.be.false;
    }));
    it("should correctly determine that symmetric-shadowcasting is symmetric on 25-percent-1", () => __awaiter(this, void 0, void 0, function* () {
        // given
        const map = new FovMap_1.FovMap(path.normalize(`${__dirname}/../../maps/25-percent-1.map`));
        const wrapper = new SymmetricShadowcastingWrapperFactory_1.SymmetricShadowcastingWrapperFactory().build(map);
        const test = new SymmetryTest_1.SymmetryTest();
        // when
        yield test.runAndSaveResult(map, wrapper);
        // then
        const result = test.results[0].results[0];
        chai_1.expect(result.errors).to.equal(0);
        chai_1.expect(result.errorPercentage).to.equal(0);
    }));
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3ltbWV0cnlDYWxjdWxhdG9yLnNwZWMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJTeW1tZXRyeUNhbGN1bGF0b3Iuc3BlYy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEsK0JBQThCO0FBQzlCLGlCQUFlO0FBQ2YsNkJBQTZCO0FBQzdCLDJIQUF3SDtBQUN4SCw2Q0FBMEM7QUFFMUMsK0RBQTREO0FBRTVELFFBQVEsQ0FBQyxvQkFBb0IsRUFBRSxHQUFHLEVBQUU7SUFDbkMsRUFBRSxDQUFDLGlGQUFpRixFQUFFLEdBQVMsRUFBRTtRQUNoRyxRQUFRO1FBQ1IsTUFBTSxHQUFHLEdBQVcsSUFBSSxlQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLFNBQVMsOEJBQThCLENBQUMsQ0FBQyxDQUFDO1FBQzNGLE1BQU0sT0FBTyxHQUFHLElBQUksMkVBQW9DLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEUsTUFBTSxVQUFVLEdBQWlCLElBQUksMkJBQVksRUFBRSxDQUFDO1FBRXBELE9BQU87UUFDUCxNQUFNLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFFaEQsT0FBTztRQUNQLGFBQU0sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7SUFDbkQsQ0FBQyxDQUFBLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQyxzRkFBc0YsRUFBRSxHQUFTLEVBQUU7UUFDckcsUUFBUTtRQUNSLE1BQU0sR0FBRyxHQUFXLElBQUksZUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxTQUFTLDhCQUE4QixDQUFDLENBQUMsQ0FBQztRQUMzRixNQUFNLE9BQU8sR0FBRyxJQUFJLDJFQUFvQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3RFLE1BQU0sSUFBSSxHQUFpQixJQUFJLDJCQUFZLEVBQUUsQ0FBQztRQUU5QyxPQUFPO1FBQ1AsTUFBTSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBRTFDLE9BQU87UUFDUCxNQUFNLE1BQU0sR0FBYSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNwRCxhQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEMsYUFBTSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzVDLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFDSixDQUFDLENBQUMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGV4cGVjdCB9IGZyb20gXCJjaGFpXCI7XG5pbXBvcnQgXCJtb2NoYVwiO1xuaW1wb3J0ICogYXMgcGF0aCBmcm9tIFwicGF0aFwiO1xuaW1wb3J0IHsgU3ltbWV0cmljU2hhZG93Y2FzdGluZ1dyYXBwZXJGYWN0b3J5IH0gZnJvbSBcIi4uLy4uL3NyYy9hbGdvcml0aG1XcmFwcGVycy9TeW1tZXRyaWNTaGFkb3djYXN0aW5nV3JhcHBlckZhY3RvcnlcIjtcbmltcG9ydCB7IEZvdk1hcCB9IGZyb20gXCIuLi8uLi9zcmMvRm92TWFwXCI7XG5pbXBvcnQgeyBTeW1tZXRyeSB9IGZyb20gXCIuLi8uLi9zcmMvUmVzdWx0c1wiO1xuaW1wb3J0IHsgU3ltbWV0cnlUZXN0IH0gZnJvbSBcIi4uLy4uL3NyYy90ZXN0cy9TeW1tZXRyeVRlc3RcIjtcblxuZGVzY3JpYmUoXCJTeW1tZXRyeUNhbGN1bGF0b3JcIiwgKCkgPT4ge1xuXHRpdChcInNob3VsZCBjb3JyZWN0bHkgY2FsY3VsYXRlIHN5bW1ldHJ5IG9uIDI1LXBlcmNlbnQtMSBmb3Igc3ltbWV0cmljLXNoYWRvd2Nhc3RpbmdcIiwgYXN5bmMgKCkgPT4ge1xuXHRcdC8vIGdpdmVuXG5cdFx0Y29uc3QgbWFwOiBGb3ZNYXAgPSBuZXcgRm92TWFwKHBhdGgubm9ybWFsaXplKGAke19fZGlybmFtZX0vLi4vLi4vbWFwcy8yNS1wZXJjZW50LTEubWFwYCkpO1xuXHRcdGNvbnN0IHdyYXBwZXIgPSBuZXcgU3ltbWV0cmljU2hhZG93Y2FzdGluZ1dyYXBwZXJGYWN0b3J5KCkuYnVpbGQobWFwKTtcblx0XHRjb25zdCBjYWxjdWxhdG9yOiBTeW1tZXRyeVRlc3QgPSBuZXcgU3ltbWV0cnlUZXN0KCk7XG5cblx0XHQvLyB3aGVuXG5cdFx0YXdhaXQgY2FsY3VsYXRvci5ydW5BbmRTYXZlUmVzdWx0KG1hcCwgd3JhcHBlcik7XG5cblx0XHQvLyB0aGVuXG5cdFx0ZXhwZWN0KG1hcC5pc09yaWdpblZpc2libGVGcm9tKDEsIDApKS50by5iZS5mYWxzZTtcblx0fSk7XG5cblx0aXQoXCJzaG91bGQgY29ycmVjdGx5IGRldGVybWluZSB0aGF0IHN5bW1ldHJpYy1zaGFkb3djYXN0aW5nIGlzIHN5bW1ldHJpYyBvbiAyNS1wZXJjZW50LTFcIiwgYXN5bmMgKCkgPT4ge1xuXHRcdC8vIGdpdmVuXG5cdFx0Y29uc3QgbWFwOiBGb3ZNYXAgPSBuZXcgRm92TWFwKHBhdGgubm9ybWFsaXplKGAke19fZGlybmFtZX0vLi4vLi4vbWFwcy8yNS1wZXJjZW50LTEubWFwYCkpO1xuXHRcdGNvbnN0IHdyYXBwZXIgPSBuZXcgU3ltbWV0cmljU2hhZG93Y2FzdGluZ1dyYXBwZXJGYWN0b3J5KCkuYnVpbGQobWFwKTtcblx0XHRjb25zdCB0ZXN0OiBTeW1tZXRyeVRlc3QgPSBuZXcgU3ltbWV0cnlUZXN0KCk7XG5cblx0XHQvLyB3aGVuXG5cdFx0YXdhaXQgdGVzdC5ydW5BbmRTYXZlUmVzdWx0KG1hcCwgd3JhcHBlcik7XG5cblx0XHQvLyB0aGVuXG5cdFx0Y29uc3QgcmVzdWx0OiBTeW1tZXRyeSA9IHRlc3QucmVzdWx0c1swXS5yZXN1bHRzWzBdO1xuXHRcdGV4cGVjdChyZXN1bHQuZXJyb3JzKS50by5lcXVhbCgwKTtcblx0XHRleHBlY3QocmVzdWx0LmVycm9yUGVyY2VudGFnZSkudG8uZXF1YWwoMCk7XG5cdH0pO1xufSk7XG4iXX0=