import { expect } from "chai";
import "mocha";
import * as path from "path";
import { SymmetricShadowcastingWrapperFactory } from "../../src/algorithmWrappers/SymmetricShadowcastingWrapperFactory";
import { FovMap } from "../../src/FovMap";
import { Symmetry } from "../../src/Results";
import { SymmetryTest } from "../../src/tests/SymmetryTest";

describe("SymmetryCalculator", () => {
	it("should correctly calculate symmetry on 25-percent-1 for symmetric-shadowcasting", async () => {
		// given
		const map: FovMap = new FovMap(path.normalize(`${__dirname}/../../maps/25-percent-1.map`));
		const wrapper = new SymmetricShadowcastingWrapperFactory().build(map);
		const calculator: SymmetryTest = new SymmetryTest();

		// when
		await calculator.runAndSaveResult(map, wrapper);

		// then
		expect(map.isOriginVisibleFrom(1, 0)).to.be.false;
	});

	it("should correctly determine that symmetric-shadowcasting is symmetric on 25-percent-1", async () => {
		// given
		const map: FovMap = new FovMap(path.normalize(`${__dirname}/../../maps/25-percent-1.map`));
		const wrapper = new SymmetricShadowcastingWrapperFactory().build(map);
		const test: SymmetryTest = new SymmetryTest();

		// when
		await test.runAndSaveResult(map, wrapper);

		// then
		const result: Symmetry = test.results[0].results[0];
		expect(result.errors).to.equal(0);
		expect(result.errorPercentage).to.equal(0);
	});
});
