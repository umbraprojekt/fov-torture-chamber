# FOV Torture Chamber

A suite of tests for field of view (FOV) algorithms.

## Included tests

Name           | Normalised name | Description
-------------- | --------------- | -----------
Benchmark      | `benchmark`     | calculates each algorithm on each map repeatedly over a period of time. Outputs the average calculation time in microseconds.
Symmetry       | `symmetry`      | calculates the percentage of asymmetric cells (cases where cell A can see B, but cell B cannot see A).
Image          | `image`         | generates an image of the calculated FOV on each map
Symmetry image | `symmetryImage` | same as `image`, but marks asymmetric cells as well

## Included algorithms

Name                    | Normalised name | Source
----------------------- | --------------- | ------
MRPAS                   | `mrpas`         | https://www.npmjs.com/package/mrpas
Permissive FOV          | `permissive`    | https://www.npmjs.com/package/permissive-fov
MRPAS.js                | `mrpas-js`      | https://github.com/domasx2/mrpas-js
Symmetric Shadowcasting | `sym-shadow`    | https://gist.github.com/as-f/59bb06ced7e740e11ec7dda9d82717f6
Discrete Shadowcasting  | `rot-discrete`  | https://www.npmjs.com/package/rot-js
Precise Shadowcasting   | `rot-precise`   | https://www.npmjs.com/package/rot-js
Recursive Shadowcasting | `rot-recursive` | https://www.npmjs.com/package/rot-js


## Running

The simple way:

```
npm install
npm start
```

The slightly more complicated way:

```
npm install
npm run build
node src/index.js
```

## Command line arguments

When running `node src/index.js`, optional arguments can be specified:

Argument       | Argument (alt) | Default | Values
-------------- | -------------- | ------- | ---------------
`--maps`       | `-m`           | `all`   | Comma-separated list of map names (maps contained in the `maps/` directory, excluding extension, e.g. `outdoor,indoor`). Possible special values: `all`, `pillars`, `corners`, `indoors`, `outdoors`, `base`.
`--algorithms` | `-a`           | `all`   | Comma-separated list of algorithm normalised names. Special value: `all`.
`--tests`      | `-t`           | `all`   | Comma-separated list of test normalised names.
`--time`       | `-T`           | `5000`  | Duration of a single benchmark test in milliseconds.

Example:

```
node src/index.js \
	--maps outdoors,indoors,corner,full,empty \
	--algorithms mrpas,permissive,sym-shadow \
	--tests benchmark,image \
	--time 1000
```
